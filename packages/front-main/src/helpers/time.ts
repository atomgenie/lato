import { Datetime } from '../types/time';

export const getFullDay = (date: Datetime): string => {
    return date.format("dddd, MMMM DD")
}

export const getTime = (date: Datetime): string => {
    return date.format("H:mm")
}

export const getDay = (date: Datetime): string => {
    return date.format("dddd");
}

export const getMonthDay = (date: Datetime): string => {
    return date.format("MMMM DD");
}