import { useState, useEffect, useCallback, useRef, RefObject } from 'react'
import { useSpring } from 'react-spring'

const CLOSED_DIFF = 50;
const OPEN_DIFF = 50;

enum OPEN_STATE {
    OPEN,
    CLOSE,
}

interface TimedPosition {
    y: number,
    timestamp: number,
}

export const useDraggable = (
    defaultOpen: boolean,
    open: boolean,
    opennable: boolean,
    stopScroll?: HTMLElement | null,
    onClose?: () => void,
): {
        positionY: number | string | undefined,
        openned: boolean,
        htmlRef: RefObject<HTMLDivElement>,
        handleDrag: (newPositionY: number) => void,
        handleStartDrag: (startDrag: number) => void,
        handleEndDrag: () => void,
        setHasTension: (hasTension: boolean) => void,
        fullyOpenned: boolean,
    } => {

    const divRef = useRef<HTMLDivElement>(null)
    const [realPositionY, setRealPositionY] = useState<number>(window.innerHeight + 200)
    const [previousState, setPreviousState] = useState<OPEN_STATE>(OPEN_STATE.CLOSE)
    const [openned, setOpenned] = useState<boolean>(false)
    const [hasTension, setHasTension] = useState<boolean>(true)
    const [diffPos, setDiffPos] = useState<number>(0)
    const [fullyOpenned, setFullyOpenned] = useState<boolean>(false)
    const [lastPosY, setLastPosY] = useState<TimedPosition | undefined>(undefined)

    // Animation on default open
    useEffect(() => {
        if (defaultOpen) {
            setTimeout(() => {
                setRealPositionY(OPEN_DIFF)
                setOpenned(true)
                setPreviousState(OPEN_STATE.OPEN)

            }, 100)
        } else if (opennable) {
            setTimeout(() => setRealPositionY(window.innerHeight - CLOSED_DIFF), 200)
        }
    }, [defaultOpen, opennable])

    // Handle stop scrolling on parent element
    useEffect(() => {
        if (!stopScroll) {
            return
        }
        if (openned) {
            stopScroll.style.overflow = "hidden"
        } else {
            stopScroll.style.overflow = "initial"
        }
    }, [openned, stopScroll])

    const smoothSpring = useSpring({
        top: realPositionY,
        immediate: !hasTension,
        config: {
            tension: 300,
        },
    })

    // Detect if the drag is fully openned
    useEffect(() => {
        let full: boolean
        if ((smoothSpring.top as any).value < OPEN_DIFF) {
            full = true
        } else {
            full = false
        }
        if (full !== fullyOpenned) {
            setFullyOpenned(full)
        }
    }, [smoothSpring, fullyOpenned])

    const handleStartDrag = useCallback((posY: number) => {

        setDiffPos(posY - realPositionY)
        setOpenned(true)

    }, [realPositionY])

    const handleCloseOrOpen = useCallback((open: boolean) => {
        if (open) {
            setRealPositionY(OPEN_DIFF)
            setOpenned(true)
            setPreviousState(OPEN_STATE.OPEN)
        } else {
            if (opennable) {
                setRealPositionY(window.innerHeight - CLOSED_DIFF)
            } else {
                setRealPositionY(window.innerHeight + 200)
            }
            setPreviousState(OPEN_STATE.CLOSE)
            setOpenned(false)
            onClose && onClose()
        }
    }, [opennable, onClose])

    // Handle open props changing
    useEffect(() => {
        handleCloseOrOpen(open)

        //eslint-disable-next-line react-hooks/exhaustive-deps
    }, [open])

    const handleEndDrag = useCallback(() => {

        setHasTension(true)
        if (realPositionY < (window.innerHeight * (previousState === OPEN_STATE.OPEN ? 1 : 3)) / 4) {
            if (realPositionY < OPEN_DIFF) {
                if (lastPosY) {
                    const delta = realPositionY - lastPosY.y
                    const now = Date.now()

                    const newPosition = realPositionY + ((delta * 200) / (now - lastPosY.timestamp))
                    if (divRef.current && newPosition < -divRef.current.clientHeight + window.innerHeight) {
                        setRealPositionY(-divRef.current.clientHeight + window.innerHeight)
                        return
                    }
                    else if (newPosition > OPEN_DIFF) {
                        setRealPositionY(OPEN_DIFF)
                    }
                    else {
                        setRealPositionY(newPosition)
                    }
                }

                return
            }
            handleCloseOrOpen(true)

        } else {
            handleCloseOrOpen(false)
        }

    }, [handleCloseOrOpen, previousState, realPositionY, lastPosY])

    const handleDrag = useCallback((posY: number) => {
        const yNewPos = posY - diffPos

        if (divRef.current && yNewPos < -divRef.current.clientHeight + window.innerHeight) {
            setRealPositionY(-divRef.current.clientHeight + window.innerHeight)
            return
        }

        setLastPosY({
            y: realPositionY,
            timestamp: Date.now()
        })

        setHasTension(false)

        setRealPositionY(yNewPos)

    }, [diffPos, divRef, realPositionY])

    return {
        positionY: smoothSpring.top,
        openned: openned,
        htmlRef: divRef,
        handleDrag: handleDrag,
        handleStartDrag: handleStartDrag,
        handleEndDrag: handleEndDrag,
        setHasTension: setHasTension,
        fullyOpenned: fullyOpenned,
    }

}

export const useNaturalScroll = () => {

}