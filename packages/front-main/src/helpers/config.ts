export const OPEN_MAIN = !!localStorage.getItem('OPEN_MAIN') ? true : false;

export const setOpenMain = (value: boolean) => {
    if (value) {
        localStorage.setItem("OPEN_MAIN", "true");
    } else {
        localStorage.removeItem("OPEN_MAIN");
    }
}
