import React, { useState, useEffect } from "react"
import { useDraggable } from "../helpers/gesture"
import styles from "./Dragable.module.scss"
import { animated } from "react-spring"

interface props {
    stopScroll?: HTMLElement | null
    open?: boolean
    onClose?: () => void
}

const Dragable: React.FC<props> = ({ stopScroll, children, open, onClose }) => {
    const [openDrag, setOpenDrag] = useState<boolean>(open !== undefined && open)

    const handleClick = (e: React.MouseEvent) => {
        e.stopPropagation()
        setOpenDrag(!openDrag)
    }

    const {
        positionY,
        handleDrag,
        handleStartDrag,
        handleEndDrag,
        htmlRef,
    } = useDraggable(false, openDrag, open === undefined, stopScroll, onClose)

    useEffect(() => {
        if (open !== undefined) {
            setOpenDrag(open)
        }
    }, [open])

    return (
        <animated.div
            className={styles.dragable}
            style={{
                top: positionY,
            }}
            onTouchStart={e => {
                e.stopPropagation()
                handleStartDrag(e.touches[0].clientY)
            }}
            onTouchMove={e => {
                e.stopPropagation()
                handleDrag(e.touches[0].clientY)
            }}
            onTouchEnd={e => {
                handleEndDrag()
            }}
            ref={htmlRef}
        >
            <div className={styles.topPosition}>
                <div className={styles.clickableDragBtn} onClick={handleClick}>
                    <div className={styles.dragBtn} />
                </div>
                <div className={styles.contain}>{children}</div>
            </div>
        </animated.div>
    )
}

export default Dragable
