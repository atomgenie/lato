import React, { useState, useCallback, useEffect } from "react"
import styles from "./CalendarView.module.scss"
import Calendar from "./Calendar"
import Dragable from "./Dragable"
import FocusLecture from "./FocusLecture"
import { Schedule } from "../types/schedule"
import { useSpring, animated } from "react-spring"
import SelectWeek from "./SelectWeek"
import { EpitaProvider } from "../fetchers/epita/epita-provider"
import moment from "moment"

const WIDTH = window.innerWidth

const MAX_PANEL = 3

const CalendarView: React.FC = () => {
    const [focusedLecture, setFocusedLecture] = useState<Schedule | undefined>(undefined)
    const [openFocus, setOpenFocus] = useState<boolean>(false)
    const [panel, setPanel] = useState<number>(0)
    const [loading, setLoading] = useState<boolean>(true)

    const [posiX, setPosiX] = useState<number>(0)
    const [diffX, setDiffX] = useState<number>(0)
    const [startPos, setStartPos] = useState<[number, number]>([0, 0])
    const [allowSwipe, setAllowSwipe] = useState<boolean>(false)

    const [{ x }, set] = useSpring<{ x: number }>(() => ({ x: panel * WIDTH }))

    const handleStartSwipe = useCallback(
        (touchEvent: React.TouchEvent) => {
            const fromLeft = touchEvent.touches[0].clientX
            setDiffX(posiX + fromLeft)
            setStartPos([fromLeft, touchEvent.touches[0].clientY])
            setAllowSwipe(false)
        },
        [posiX],
    )

    const handleSwipe = useCallback(
        (touchEvent: React.TouchEvent) => {
            let allowSwipeNow = allowSwipe
            const actualPosition = [
                touchEvent.touches[0].clientX,
                touchEvent.touches[0].clientY,
            ]
            if (!allowSwipe) {
                if (
                    Math.abs(actualPosition[0] - startPos[0]) > 20 &&
                    Math.abs(actualPosition[1] - startPos[1]) < 30
                ) {
                    setAllowSwipe(true)
                    allowSwipeNow = true
                }
                if (!allowSwipeNow) {
                    return
                }
            }
            touchEvent.stopPropagation()
            let calculatedPosi = diffX - actualPosition[0]
            if (calculatedPosi < 0) {
                calculatedPosi = 0
            }
            if (calculatedPosi > (MAX_PANEL - 1) * WIDTH) {
                calculatedPosi = (MAX_PANEL - 1) * WIDTH
            }
            set({
                x: calculatedPosi,
                immediate: false,
            })
            setPosiX(calculatedPosi)
        },
        [allowSwipe, diffX, set, startPos],
    )

    const handleEndSwipe = useCallback(() => {
        if (!allowSwipe) {
            return
        }

        const pannelNumber = Math.round(Math.abs(posiX / WIDTH))
        set({
            x: pannelNumber * WIDTH,
            immediate: false,
        })
        setPosiX(pannelNumber * WIDTH)
        setPanel(pannelNumber)
    }, [allowSwipe, posiX, set])

    const handleChangePanel = useCallback(
        (panel: number) => {
            if (panel < 0) {
                panel = 0
            } else if (panel >= MAX_PANEL) {
                panel = MAX_PANEL - 1
            }
            set({
                x: WIDTH * panel,
            })
            setPanel(panel)
            setPosiX(WIDTH * panel)
        },
        [set],
    )

    const [schedules, setSchedules] = useState<Schedule[][]>([])

    useEffect(() => {
        const getSchedules = async () => {
            setLoading(true)
            const epitaProvider = new EpitaProvider()
            const allSchedules = await epitaProvider.getWeekCurse(moment())
            setSchedules(allSchedules)
            setLoading(false)
        }

        getSchedules()
    }, [])

    return (
        <>
            <SelectWeek
                onClickLeft={() => handleChangePanel(panel - 1)}
                onClickRight={() => handleChangePanel(panel + 1)}
            />
            <animated.div
                onTouchStart={handleStartSwipe}
                onTouchMove={handleSwipe}
                onTouchEnd={handleEndSwipe}
                style={{
                    right: x,
                }}
                className={styles.carussel}
            >
                {loading ? (
                    <div className={styles.loading}>Chargement...</div>
                ) : (
                    <>
                        <div className={styles.pannel}>
                            {schedules.map((schedule, i) => (
                                <Calendar
                                    key={i}
                                    setFocusLecture={setFocusedLecture}
                                    setOpen={() => setOpenFocus(true)}
                                    courses={schedule}
                                />
                            ))}
                        </div>
                        <div className={styles.pannel}>
                            {schedules.map((schedule, i) => (
                                <Calendar
                                    key={i}
                                    setFocusLecture={setFocusedLecture}
                                    setOpen={() => setOpenFocus(true)}
                                    courses={schedule}
                                />
                            ))}
                        </div>
                        <div className={styles.pannel}>
                            {schedules.map((schedule, i) => (
                                <Calendar
                                    key={i}
                                    setFocusLecture={setFocusedLecture}
                                    setOpen={() => setOpenFocus(true)}
                                    courses={schedule}
                                />
                            ))}
                        </div>
                    </>
                )}
            </animated.div>

            <Dragable
                open={openFocus}
                onClose={() => {
                    setOpenFocus(false)
                }}
            >
                <>{focusedLecture && <FocusLecture data={focusedLecture} />}</>
            </Dragable>
        </>
    )
}

export default CalendarView
