import React from 'react';
import styles from "./Calendar.module.scss";
import CalendarItem from './CalendarItem';
import { Schedule } from '../types/schedule';
import moment from "moment";

interface props {
    courses: Schedule[]
    setFocusLecture?: (lecture: Schedule) => void,
    setOpen?: () => void
}

const Calendar: React.FC<props> = ({
    setFocusLecture,
    setOpen,
    courses
}) => {

    return (
        <div className={"container " + styles.main}>


            <CalendarItem data={courses}
                day={courses[0] ? courses[0].start : moment()}
                setFocusLecture={setFocusLecture}
                setOpen={setOpen}
            />

        </div>
    )
}

export default Calendar;