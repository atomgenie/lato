import React from 'react';
import styles from './Tag.module.scss';

const Tag: React.FC = ({ children }) => {
    return (
        <div className={styles.tag}>{children}</div>
    )
}

export const TagGroup: React.FC<{
    className?: string
}> = ({ children, className }) => {
    const rootClass = styles.tagGroup + " " + (className || "");
    return (
        <div className={rootClass}>
            {children}
        </div>
    )
}

export default Tag;