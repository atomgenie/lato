import React from "react"
import styles from "./Cursor.module.scss"

interface props {
    value?: boolean
    onChange?: (value: boolean) => void
    className?: string
}

const Cursor: React.FC<props> = ({ value, onChange, className }) => {
    const rootName = styles.rootElm + " " + (className || "")
    const rightPos = value ? 10 : 25

    return (
        <div className={rootName}>
            <div className={styles.line} />
            <div
                className={styles.buttonElm}
                style={{
                    right: rightPos,
                }}
                onClick={() => {
                    onChange && onChange(!value)
                }}
            >
                <div
                    className={styles.miniButtonElm}
                    style={{
                        backgroundColor: value ? "#001f6b" : undefined,
                    }}
                    onClick={() => {
                        onChange && onChange(!value)
                    }}
                />
            </div>
        </div>
    )
}

export default Cursor
