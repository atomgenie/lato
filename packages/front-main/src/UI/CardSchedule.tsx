import React from "react"
import styles from "./CardSchedule.module.scss"
import { ScheduleLight } from "../types/schedule"
import { getFullDay, getTime } from "../helpers/time"
import Tag, { TagGroup } from "./Tag"
import RightBlueArrow from "./rightBueArrow.svg"

interface props {
    data: ScheduleLight
    onClick?: (e: React.MouseEvent) => void
    className?: string
}

export const ScheduleWhite: React.FC<props> = ({ data, className, onClick }) => {
    const rootClass = styles.whiteSchedule + " " + (className || "")
    return (
        <div className={rootClass} onClick={onClick}>
            <div className={styles.day}>{getFullDay(data.start)}</div>
            <div className={styles.hour}>{getTime(data.start)}</div>
            <div className={styles.card}>
                <div className="columns is-mobile is-vcentered">
                    <div className="column is-10">
                        <div className={styles.title}>{data.name}</div>
                        <div className={styles.room}>
                            {data.rooms.map(room => (
                                <Tag key="room">{room}</Tag>
                            ))}
                        </div>
                        <div className={styles.groups}>
                            {data.groups.map(group => (
                                <TagGroup key={group.id} className={styles.miniGroup}>
                                    {group.name}
                                </TagGroup>
                            ))}
                        </div>
                    </div>
                    <div className={styles.rightPart + " column is-2"}>
                        <img
                            src={RightBlueArrow}
                            alt="Logo click"
                            className={styles.miniBlueArrow}
                        />
                    </div>
                </div>
            </div>
            <div className={styles.hour}>{getTime(data.end)}</div>
        </div>
    )
}

export const ScheduleMini: React.FC<props> = ({ data, className, onClick }) => {
    const rootClass = styles.miniCard + " " + (className || "")

    return (
        <div
            className={rootClass}
            onClick={e => {
                onClick && onClick(e)
            }}
        >
            <div className={styles.miniCardTitle}>{data.name}</div>
            <div className={styles.room}>
                {data.rooms.map(room => (
                    <div className={styles.roomNames} key={room}>
                        <Tag>{room}</Tag>
                    </div>
                ))}
            </div>
            <div className={styles.groups}>
                {data.groups.slice(0, 3).map(group => (
                    <TagGroup key={group.id} className={styles.miniGroup}>
                        {group.name}
                    </TagGroup>
                ))}
                {data.groups.length > 3 && (
                    <TagGroup className={styles.miniGroup}>...</TagGroup>
                )}
            </div>
        </div>
    )
}
