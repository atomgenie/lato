import React from 'react';
import styles from "./Pin.module.scss";

export const BigPin: React.FC = () => {
    return (
        <div className={styles.bigPin}>
            <div className={styles.insideBigPin}/>
        </div>
    )
}

export const Pin: React.FC = () => {
    return (
        <div className={styles.pin}>
            <div className={styles.insidePin}/>
        </div>
    )
}

