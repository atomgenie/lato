import React from 'react';
import styles from "./CalendarItem.module.scss";
import { Schedule } from '../types/schedule';
import { getDay, getMonthDay, getTime } from '../helpers/time';
import { Datetime } from '../types/time';
import { BigPin, Pin } from './Pin';
import { ScheduleMini } from './CardSchedule';

interface props {
    data: Schedule[],
    day: Datetime,
    setFocusLecture?: (ecture: Schedule) => void,
    setOpen?: () => void,
}

const CalendarItem: React.FC<props> = ({
    data,
    day,
    setFocusLecture,
    setOpen,
 }) => {


    return (
        <div className={styles.theTable}>
            <table className={styles.table}>
                <tbody>
                    <tr>
                        <td className={styles.firstTd}>
                            <div className={styles.line} />
                        </td>
                        <td>
                            <table className={styles.table + " " + styles.leftPanel}>
                                <tbody>
                                    <tr>
                                        <td className={styles.leftPins}>
                                            <BigPin />
                                        </td>
                                        <td className={styles.rightTd}>
                                            <div className={styles.titleDay}>{getDay(day)}</div>
                                            <div className={styles.titleMonth}>{getMonthDay(day)}</div>
                                        </td>
                                    </tr>
                                    {data.map((elm, pos) =>
                                        <tr key={elm.id}>
                                            <td className={styles.leftPins}>
                                                <Pin />
                                            </td>
                                            <td className={styles.rightTd}>
                                                <div className={styles.beginHour}>{getTime(elm.start)}</div>
                                                <div className={styles.card}>
                                                    <ScheduleMini data={elm} onClick={() => {
                                                        setFocusLecture && setFocusLecture(elm);
                                                        setOpen && setOpen();
                                                    }}/>
                                                </div>
                                                {(pos === data.length - 1 || !elm.end.isSame(data[pos + 1].start)) &&
                                                    <div className={styles.endHour}>{getTime(elm.end)}</div>
                                                }
                                            </td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    )
}

export default CalendarItem;
