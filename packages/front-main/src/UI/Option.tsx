import React from 'react'
import styles from "./Option.module.scss"
import Cursor from "./Cursor"

interface props {
    value: boolean
    className?: string,
    onChange?: (value: boolean) => void,
}

const Option: React.FC<props> = ({
    value,
    className,
    children,
    onChange
}) => {

    const rootClass = styles.option + " " + (className || "")

    return (
        <div className={rootClass}>
            <div className="columns is-mobile is-vcentered">
                <div className="column is-10">
                    {children}
                </div>
                <div className={"column is-2 " + styles.button}>
                    <Cursor value={value} onChange={onChange} />
                </div>
            </div>
        </div>
    )
}

export default Option;