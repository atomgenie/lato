import React from 'react'
import styles from "./SelectWeek.module.scss"
import LeftArrow from "./left-dark-blue-arrow.svg"
import RightArrow from "./right-dark-blue-arrow.svg"


interface props {
    onClickRight?: () => void
    onClickLeft?: () => void
}

const SelectWeek: React.FC<props> = ({
    onClickLeft,
    onClickRight,
}) => {

    return (
        <div className={styles.rootElm}>
            <div className={styles.content} onClick={onClickLeft}>
                <img src={LeftArrow} alt="Left Arrow" className={styles.arrow} />
                <div>
                    Précédent
                </div>
            </div>
            <div className={styles.content} onClick={onClickRight}>
                <div>
                    Suivant
                </div>
                <img src={RightArrow} alt="Right Arrow" className={styles.arrow} />
            </div>
        </div>
    )
}

export default SelectWeek