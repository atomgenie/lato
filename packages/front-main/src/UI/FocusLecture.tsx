import React from 'react';
import styles from "./FocusLecture.module.scss";

import Tag, { TagGroup } from './Tag';
import { Schedule } from '../types/schedule';
import { getTime, getMonthDay, getDay } from '../helpers/time';

interface props {
    data: Schedule,
}

const FocusLecture: React.FC<props> = ({
    data,
}) => {
    return (
        <div className={"container " + styles.rootElm}>
            <h2 className={styles.h2}>
                {data.name}
            </h2>
            <div className={styles.timezone}>
                <table className={styles.table}>
                    <tbody>
                        <tr>
                            <td className={styles.tdTime}>
                                <div className={styles.timed}>
                                    {getTime(data.start)}
                                </div>
                            </td>
                            <td className={styles.tdDate}>
                                <div className={styles.nameDay}>
                                    {getDay(data.start)}
                                </div>
                                <div className={styles.nameMonth}>
                                    {getMonthDay(data.start)}
                                </div>
                            </td>
                            <td className={styles.tdTime}>
                                <div className={styles.timed}>
                                    {getTime(data.end)}
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className={styles.tagRoom}>
                { data.rooms.map(room => <div className={styles.roomName} key={room}><Tag>{room}</Tag></div>) }
            </div>
            <div className={styles.groups}>
                {data.groups.map(group =>
                    <TagGroup key={group.id} className={styles.miniGroup}>{group.name}</TagGroup>
                )}
            </div>
            <div className={styles.notes}>
                <div className={styles.titleNote}>
                    Notes
                </div>
                <div className={styles.noteData}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br />
                    Fusce auctor id ante eu varius. Aliquam convallis magna et <br />
                    neque convallis, vel cursus tellus accumsan. <br />
                    Phasellus a mauris venenatis, varius neque et, condimentum ex. <br /> <br />
                    Nunc laoreet imperdiet velit, et aliquam leo bibendum eu. <br />
                    Integer vestibulum lobortis ligula cursus vestibulum. <br />
                    Maecenas vitae tortor quis eros ultrices ultricies.
                </div>
            </div>
        </div>
    )
}

export default FocusLecture;