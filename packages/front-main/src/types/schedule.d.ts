import { GroupLight } from "./group";
import { Datetime } from "./time";

export interface ScheduleLight {
    id: string,
    name: string,
    rooms: string[],
    groups: GroupLight[]
    start: Datetime,
    end: Datetime,
}

export interface Schedule extends ScheduleLight {
    teachers: string[],
    note: string,
}