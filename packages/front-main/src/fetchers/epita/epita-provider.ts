import axios from "axios"
import * as t from "io-ts"
import { groupBy, isString } from "lodash"
import moment, { Moment } from "moment"
import { Datetime, Schedule } from "../../types"
import { AbstractProvider } from "../abstract-provider"
import { isRight } from "fp-ts/lib/Either"
import { PathReporter } from "io-ts/lib/PathReporter"

const baseUrlProxy = "https://us-central1-lato-3f45a.cloudfunctions.net/proxy/epita"

interface ReservationPostApi {
    groups: readonly number[]
    rooms: readonly any[]
    teachers: readonly any[]
    startDate: string //"2021-10-10T22:00:00.000Z",
    endDate: string //"2021-10-17T21:59:59.999Z"
}

function isDatetime(datetime: unknown): datetime is Datetime {
    return moment.isMoment(datetime) && datetime.isValid()
}

const RDatetime = new t.Type<Datetime, Datetime, unknown>(
    "datetime",
    isDatetime,
    (src, context) => {
        if (!isString(src)) {
            return t.failure(src, context)
        }

        const parsedDate = moment(src)

        if (!isDatetime(parsedDate)) {
            return t.failure(src, context)
        }

        return t.success(parsedDate)
    },
    t.identity,
)

const RApiReservations = t.readonly(
    t.array(
        t.type({
            idReservation: t.Int,
            name: t.string,
            idType: t.Int,
            startDate: RDatetime,
            endDate: RDatetime,
            isOnline: t.boolean,
            rooms: t.array(
                t.type({
                    id: t.Int,
                    capacity: t.Int,
                    name: t.string,
                    idRoomType: t.Int,
                    idLocation: t.Int,
                }),
            ),
            groups: t.array(
                t.type({
                    id: t.Int,
                    name: t.string,
                    idSchool: t.Int,
                    color: t.string,
                }),
            ),
            idSchool: t.Int,
            schoolName: t.string,
        }),
    ),
)

export class EpitaProvider extends AbstractProvider {
    public async getWeekCurse(day: Moment): Promise<Schedule[][]> {
        const startDate = moment().startOf("day")
        const endDate = startDate.clone().add(1, "week")

        const payload: ReservationPostApi = {
            groups: [9],
            rooms: [],
            teachers: [],
            startDate: startDate.toISOString(),
            endDate: endDate.toISOString(),
        }

        const { data } = await axios.post<unknown>(
            `${baseUrlProxy}/reservation/filter/displayable`,
            payload,
        )

        const decodedData = RApiReservations.decode(data)

        if (!isRight(decodedData)) {
            console.error("Invalid data", PathReporter.report(decodedData))
            return []
        }

        const courses = RApiReservations.encode(decodedData.right)

        const courseList = courses
            .map(
                (course): Schedule => ({
                    id: course.idReservation.toString(),
                    name: course.name,
                    start: course.startDate,
                    end: course.endDate,
                    groups: course.groups.map(group => ({
                        id: group.id.toString(),
                        name: group.name,
                    })),
                    note: "",
                    rooms: course.rooms.map(room => room.name),
                    teachers: [],
                }),
            )
            .sort((scheduleA, scheduleB) =>
                scheduleA.start.isAfter(scheduleB.start) ? 1 : -1,
            )

        const coursesPerDays = groupBy(courseList, schedule =>
            schedule.start.format("YYYY-MM-DD"),
        )

        return Object.values(coursesPerDays)
    }
}
