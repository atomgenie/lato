import axios from "axios"
import { Schedule } from "../../types"
import { DayModel } from "../../api-types/epita"
import { CourseApiToSchedule } from "../../converters/epita/schedule"
import moment from "moment"
import { difference } from "lodash"

export const getCourses = async (): Promise<Schedule[][]> => {
    const response = await axios.get<{
        Id: number
        DayList: DayModel[]
    }>(
        `https://us-central1-lato-3f45a.cloudfunctions.net/proxy/epita/Week/GetWeek/${6}/196/1`,
    )

    const sortedDaylist = [...response.data.DayList].sort((day1, day2) => {
        const time1 = moment(day1.DateTime)
        const time2 = moment(day2.DateTime)
        return time1.isAfter(time2) ? 1 : -1
    })

    const schedulesPromise = sortedDaylist.map(
        async day =>
            await Promise.all(day.CourseList.map(course => CourseApiToSchedule(course))),
    )

    const mixedSchedules = await Promise.all(schedulesPromise)

    const sortedSchedules = mixedSchedules.map(day => {
        return day.sort((schedule1, schedule2) => {
            const time1 = moment(schedule1.start)
            const time2 = moment(schedule2.start)

            return time1.isAfter(time2) ? 1 : -1
        })
    })

    const reducedSchedules = sortedSchedules.map(schedules =>
        schedules.reduce<Schedule[]>((prevResult, schedule) => {
            if (prevResult.length === 0) {
                return [schedule]
            }

            const prevSchedule = prevResult[prevResult.length - 1]
            if (
                prevSchedule.end.isSame(schedule.start) &&
                prevSchedule.name === schedule.name &&
                difference(
                    prevSchedule.groups.map(group => group.name),
                    schedule.groups.map(group => group.name),
                ).length === 0 &&
                difference(prevSchedule.rooms, schedule.rooms).length === 0 &&
                difference(prevSchedule.teachers, schedule.teachers).length === 0
            ) {
                const newArray = [...prevResult]
                newArray[newArray.length - 1] = {
                    ...prevSchedule,
                    end: schedule.end,
                }
                return newArray
            }
            return [...prevResult, schedule]
        }, []),
    )

    return reducedSchedules
}
