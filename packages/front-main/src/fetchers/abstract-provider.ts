import { Moment } from "moment"
import { Schedule } from "../types"

export abstract class AbstractProvider {
    protected getBeginingWeek(date: Moment): Moment {
        return date.clone().startOf("week")
    }

    /**
     * Get 7 days sechdules.
     * @param day A random day of a week
     */
    public abstract getWeekCurse(day: Moment): Promise<Schedule[][]>
}
