import React, { useState } from 'react';
import { ScheduleWhite } from './UI/CardSchedule';
import Option from './UI/Option';
import { Schedule } from './types/schedule';
import Dragable from './UI/Dragable';
import moment from 'moment';
import CalendarView from './UI/CalendarView'
import "bulma";
import styles from "./App.module.scss";
import { OPEN_MAIN, setOpenMain as setOpenMainLocalstorage } from './helpers/config';
import SoonArrow from "./soonArrow.svg"
import FocusLecture from './UI/FocusLecture';

const fakeSchedule: Schedule = {
    id: "dsdq",
    groups: [{
        id: "sqdqs",
        name: "MTI"
    },
        {
            id: "sqddqsdq",
            name: "SCIA"
        }
    ],
    start: moment(),
    end: moment().add(1, "hours"),
    name: "Présentation mineure finance",
    rooms: ["Amphi 4"],
    teachers: ["Courtois"],
    note: "",
}

const App: React.FC = () => {

    const [optionOpenMain, setOptionOpenMain] = useState<boolean>(OPEN_MAIN);
    const [openMain, setOpenMain] = useState<true | undefined>(OPEN_MAIN || undefined)
    const [focusOpen, setFocusOpen] = useState<boolean>(false)
    const [focusedSchedule, setFocusedSchedule] = useState<undefined | Schedule>(undefined)
    const handleOpenMainOption = (value: boolean) => {
        setOptionOpenMain(value);
        setOpenMainLocalstorage(value)
    }

    return (
        <div>
            <div className={styles.app}>
                <div className="container">
                    <h1 className={styles.appName}>
                        Lato (beta)
                    </h1>
                    <div className={styles.soon}>
                        <img src={SoonArrow} alt="Soon Arrow" className={styles.soonArrow}/>
                        Soon
                    </div>
                    <ScheduleWhite
                        data={fakeSchedule}
                        onClick={() => {
                            setFocusOpen(true)
                            setFocusedSchedule(fakeSchedule)
                        }}
                    />
                    <div className={styles.options}>
                        <div className={styles.titleOption}>
                            Options
                        </div>
                        <div className="columns is-multiline is-1 is-variable">
                            <div className={"column is-6 " + styles.optColumn}>
                                <Option value={optionOpenMain} onChange={handleOpenMainOption} className={styles.option}>Ouvrir le calendrier à l'ouverture</Option>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <Dragable
                open={focusOpen}
                onClose={() => {
                    setFocusOpen(false)
                }}
                stopScroll={document.documentElement}
            >
                <>
                    {focusedSchedule && <FocusLecture data={focusedSchedule} />}
                </>
            </Dragable>

            <Dragable
                stopScroll={document.documentElement}
                open={openMain}
                onClose={() => {
                    setOpenMain(undefined)
                }}
            >
                <CalendarView />
            </Dragable>
        </div>
    );
}

export default App;
