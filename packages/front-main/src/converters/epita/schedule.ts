import { CourseModel } from "../../api-types/epita"
import { Schedule } from "../../types"
import { groupApiToLato } from "./group"
import moment from "moment"

export const CourseApiToSchedule = async (course: CourseModel): Promise<Schedule> => {
    return {
        id: String(course.Id),
        name: course.Name,
        groups: await Promise.all(course.GroupList.map(groupApiToLato)),
        note: "",
        rooms: course.RoomList.map(room => room.Name),
        teachers: course.StaffList.map(staff => staff.Name),
        start: moment(course.BeginDate),
        end: moment(course.EndDate)
    }
}
