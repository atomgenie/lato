import { GroupModel } from "../../api-types/epita"
import { GroupLight } from "../../types"

export const groupApiToLato = async (group: GroupModel): Promise<GroupLight> => {
    return {
        id: String(group.Id),
        name: group.Name,
    }
}
