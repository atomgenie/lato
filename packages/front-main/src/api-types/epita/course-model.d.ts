import { StaffModel, RoomModel, GroupModel } from "./index"

export interface CourseModel {
    Id: number
    Name: string
    BeginDate: string
    EndDate: string
    Duration: number
    StaffList: StaffModel[]
    RoomList: RoomModel[]
    GroupList: GroupModel[]
    Code: string
    Type: string
    Url: string
    Info: string
}