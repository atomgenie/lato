import { ModelBase } from "./entity-model"
import { ENTITY_TYPE } from "./entity-type";

export interface StaffModel extends ModelBase {
    Type: ENTITY_TYPE.STAFF
}