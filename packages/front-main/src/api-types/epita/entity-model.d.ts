import { ENTITY_TYPE } from "./entity-type"

export interface ModelBase {
    Id: number
    Name: string
    Type: ENTITY_TYPE
    ParentId: -1 | GroupModel["id"],
}