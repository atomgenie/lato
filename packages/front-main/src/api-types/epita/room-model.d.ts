import { ModelBase } from "./entity-model"
import { ENTITY_TYPE } from "./entity-type";

export interface RoomModel extends ModelBase {
    Type: ENTITY_TYPE.ROOM
}