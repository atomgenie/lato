import { CourseModel } from "./course-model"

export interface DayModel {
    CourseList: CourseModel[]
    DateTime: string
}