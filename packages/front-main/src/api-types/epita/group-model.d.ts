import { ModelBase } from "./entity-model"
import { ENTITY_TYPE } from "./entity-type";

export interface GroupModel extends ModelBase {
    Type: ENTITY_TYPE.GROUP
}