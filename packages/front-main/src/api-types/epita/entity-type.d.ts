export enum ENTITY_TYPE {
    GROUP = 1,
    STAFF = 2,
    ROOM = 3,
}