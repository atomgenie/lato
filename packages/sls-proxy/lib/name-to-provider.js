"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.nameToProvider = void 0;
const epita_1 = require("./providers/epita");
const mapProviderToClass = {
    epita: new epita_1.EpitaProvider(),
};
const nameToProvider = (provider) => {
    const classProvider = mapProviderToClass[provider];
    if (!classProvider) {
        throw Error();
    }
    return classProvider;
};
exports.nameToProvider = nameToProvider;
//# sourceMappingURL=name-to-provider.js.map