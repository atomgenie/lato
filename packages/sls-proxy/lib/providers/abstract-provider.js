"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbstractProvider = void 0;
class AbstractProvider {
    applyUrl(config) {
        return Object.assign(Object.assign({}, config), { url: `${this.baseUrl}/${config.url}` });
    }
}
exports.AbstractProvider = AbstractProvider;
//# sourceMappingURL=abstract-provider.js.map