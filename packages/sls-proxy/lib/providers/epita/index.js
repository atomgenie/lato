"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EpitaProvider = void 0;
const abstract_provider_1 = require("../abstract-provider");
const firebase_functions_1 = require("firebase-functions");
class EpitaProvider extends abstract_provider_1.AbstractProvider {
    constructor() {
        super(...arguments);
        this.baseUrl = "https://zeus.ionis-it.com/api";
    }
    applyConfiguration(config) {
        return Object.assign(Object.assign({}, config), { headers: Object.assign(Object.assign({}, config.headers), { authorization: `Bearer ${(0, firebase_functions_1.config)().sls_proxy.epita_token}` }) });
    }
}
exports.EpitaProvider = EpitaProvider;
//# sourceMappingURL=index.js.map