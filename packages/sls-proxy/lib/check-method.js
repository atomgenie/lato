"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkMethod = void 0;
const methods = ["get", "GET", "post", "POST"];
const checkMethod = (desiredMethod) => {
    if (!desiredMethod) {
        return false;
    }
    return methods.some(method => desiredMethod === method);
};
exports.checkMethod = checkMethod;
//# sourceMappingURL=check-method.js.map