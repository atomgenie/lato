"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.proxy = void 0;
const functions = require("firebase-functions");
const express = require("express");
const cors = require("cors");
const name_to_provider_1 = require("./name-to-provider");
const axios_1 = require("axios");
const check_method_1 = require("./check-method");
const app = express();
app.use(cors());
app.all("/:provider/*", async (req, res) => {
    const provider = req.params.provider;
    if (!provider) {
        res.status(400).send();
        return;
    }
    let classProvider;
    try {
        classProvider = (0, name_to_provider_1.nameToProvider)(provider);
    }
    catch (_a) {
        res.status(404).send({ error: "Bad provider" });
        return;
    }
    if (!(0, check_method_1.checkMethod)(req.method)) {
        res.status(400).send();
        return;
    }
    const url = req.url
        .split("/")
        .filter((_, index) => index > 1)
        .join("/");
    let axiosConfig = {
        method: req.method,
        url: url,
        data: req.body,
    };
    try {
        axiosConfig = classProvider.applyUrl(axiosConfig);
        axiosConfig = classProvider.applyConfiguration(axiosConfig);
        const response = await (0, axios_1.default)(axiosConfig);
        res.send(response.data);
    }
    catch (e) {
        res.status(400).send({
            error: true,
            data: e.data || e.message || "",
            url: axiosConfig.url,
        });
        console.error(e);
        return;
    }
});
exports.proxy = functions.https.onRequest(app);
//# sourceMappingURL=index.js.map