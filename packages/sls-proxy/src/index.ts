import * as functions from "firebase-functions"
import express = require("express")
import cors = require("cors")
import { AbstractProvider } from "./providers/abstract-provider"
import { nameToProvider } from "./name-to-provider"
import axios, { AxiosRequestConfig } from "axios"
import { checkMethod } from "./check-method"

const app = express()
app.use(cors())

app.all("/:provider/*", async (req, res) => {
    const provider = req.params.provider
    if (!provider) {
        res.status(400).send()
        return
    }

    let classProvider: AbstractProvider
    try {
        classProvider = nameToProvider(provider)
    } catch {
        res.status(404).send({ error: "Bad provider" })
        return
    }

    if (!checkMethod(req.method)) {
        res.status(400).send()
        return
    }

    const url = req.url
        .split("/")
        .filter((_, index) => index > 1)
        .join("/")

    let axiosConfig: AxiosRequestConfig = {
        method: req.method,
        url: url,
        data: req.body,
    }

    try {
        axiosConfig = classProvider.applyUrl(axiosConfig)
        axiosConfig = classProvider.applyConfiguration(axiosConfig)
        const response = await axios(axiosConfig)
        res.send(response.data)
    } catch (e: any) {
        res.status(400).send({
            error: true,
            data: e.data || e.message || "",
            url: axiosConfig.url,
        })
        console.error(e)
        return
    }
})

export const proxy = functions.https.onRequest(app)
