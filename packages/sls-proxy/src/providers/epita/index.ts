import { AbstractProvider } from "../abstract-provider"
import { AxiosRequestConfig } from "axios"
import { config as fnConfig } from "firebase-functions"

export class EpitaProvider extends AbstractProvider {
    baseUrl = "https://zeus.ionis-it.com/api"

    applyConfiguration(config: AxiosRequestConfig) {
        return {
            ...config,
            headers: {
                ...config.headers,
                authorization: `Bearer ${fnConfig().sls_proxy.epita_token}`,
            },
        }
    }
}
