import { AxiosRequestConfig } from "axios"

export abstract class AbstractProvider {
    public abstract baseUrl: string

    public abstract applyConfiguration(config: AxiosRequestConfig): AxiosRequestConfig

    public applyUrl(config: AxiosRequestConfig) {
        return {
            ...config,
            url: `${this.baseUrl}/${config.url}`,
        }
    }
}
