import { AxiosRequestConfig } from "axios"

const methods = ["get", "GET", "post", "POST"] as const

export const checkMethod = (
    desiredMethod: string | undefined,
): desiredMethod is AxiosRequestConfig["method"] => {
    if (!desiredMethod) {
        return false
    }
    return methods.some(method => desiredMethod === method)
}
