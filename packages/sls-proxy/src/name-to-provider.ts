import { AbstractProvider } from "./providers/abstract-provider"
import { EpitaProvider } from "./providers/epita"

const mapProviderToClass: {
    [key: string]: AbstractProvider | undefined
} = {
    epita: new EpitaProvider(),
}

export const nameToProvider = (provider: string) => {
    const classProvider = mapProviderToClass[provider]
    if (!classProvider) {
        throw Error()
    }
    return classProvider

}
